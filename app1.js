const express = require('express');
const request = require('request');
const cheerio = require('cheerio');

const app = express();

app.get('/', function(req, res) {
  request('http://www.redditsoccerstreams.tv/index.php', function(error, response, body) {
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(body);
      const singlePageContent = $('.single-page').html();
      res.send(singlePageContent);
    } else {
      res.status(response.statusCode).send(error);
    }
  });
});

app.listen(3000, function() {
  console.log('App listening on port 3000!');
});
