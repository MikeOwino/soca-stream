const express = require("express");
const request = require("request");
const cheerio = require("cheerio");
const moment = require("moment");
const { v4: uuidv4 } = require("uuid");
const cors = require("cors");

const app = express();

app.use(
  cors({
    origin: "*",
  })
);

app.get("/", function (req, res) {
  request(
    "https://www.redditsoccerstreams.tv/index.php",
    function (error, response, body) {
      if (!error && response.statusCode == 200) {
        const $ = cheerio.load(body);
        const tableRows = $("tbody tr");
        let tableHtml = `
        <table class="table table-striped-columns table-hover border caption-top">
        <caption class="text-center fw-bold">Today's Games</caption>
          <thead>
            <tr class="border-bottom table-success">
              <th class="border-0">#</th>
              <th class="border-0">Time (EAT)</th>
              <th class="border-0">Game</th>
              <th class="border-0">Source</th>
            </tr>
          </thead>
          <tbody>
      `;
        // Add the new <head> element
        const headHtml = `
        <head>
          <link rel="shortcut icon" href="https://res.cloudinary.com/weknow-creators/image/upload/v1692479006/favicon_rjcrtv.ico" type="image/x-icon">
        </head>
      `;
        const watchLinks = []; // Array to hold unique watch links
        tableRows.each(function () {
          const text = $(this).find("td:nth-child(1)").text();
          const date = new Date(`01/01/2000 ${text}`); // use any date as a dummy date
          date.setHours(date.getHours() + 3); // add 3 hours
          const hours = date.getHours().toString().padStart(2, "0");
          const minutes = date.getMinutes().toString().padStart(2, "0");
          const formattedTime = `${hours}:${minutes}`;

          const match = $(this).find("td:nth-child(2)").text();
          const watchLink = $(this).find("td:nth-child(3) a").attr("href");
          if (watchLinks.indexOf(watchLink) === -1) {
            watchLinks.push(watchLink);
            app.get(`/watch${watchLinks.length}`, function (req, res) {
              res.redirect(watchLink);
            });
          }
          
          tableHtml += `
          <tr>
            <td>${watchLinks.indexOf(watchLink) + 1}</td>
            <td>${formattedTime}</td>
            <td>${match}</td>
            <td><a href="/watch${
              watchLinks.indexOf(watchLink) + 1
            }"><svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
            <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
            <g id="SVGRepo_iconCarrier">
              <path
                d="M16.6582 9.28638C18.098 10.1862 18.8178 10.6361 19.0647 11.2122C19.2803 11.7152 19.2803 12.2847 19.0647 12.7878C18.8178 13.3638 18.098 13.8137 16.6582 14.7136L9.896 18.94C8.29805 19.9387 7.49907 20.4381 6.83973 20.385C6.26501 20.3388 5.73818 20.0469 5.3944 19.584C5 19.053 5 18.1108 5 16.2264V7.77357C5 5.88919 5 4.94701 5.3944 4.41598C5.73818 3.9531 6.26501 3.66111 6.83973 3.6149C7.49907 3.5619 8.29805 4.06126 9.896 5.05998L16.6582 9.28638Z"
                stroke="#0008ff" stroke-width="2" stroke-linejoin="round"></path>
            </g>
          </svg> Watch Now</a></td>
          </tr>
        `;
        });
        tableHtml += `
          </tbody>
        </table>
      `;
        res.send(`
        <!doctype html>
        <html>
          <head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-cKzsVdAOMTjsiEBX9yVEI1rq2UGrjY0i+/wFzZUEGyyOrsKs0sMnKfbXKPTmHmGtSaxLbqqPzTW/zxKtzwW8gg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
            <title>Sporty TV</title>
            ${headHtml}
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.2/css/bootstrap.min.css" integrity="sha512-b2QcS5SsA8tZodcDtGRELiGv5SaKSk1vDHDaQRda0htPYWZ6046lr3kJ5bAAQdpV2mmA/4v0wQF9MyU6/pDIAg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
          </head>
          <body>
            <div class="container">
            <div style="text-align: center;">
        <a href="/"><img src="https://res.cloudinary.com/weknow-creators/image/upload/v1694118590/logo-arrena_rfimdk_ujyzxh.webp" alt="logo" height="3%"></a>
    </div>
    <br>
           
            
            <h6 style="text-align: center;"><strong>All links are pulled from <a href="https://www.redditsoccerstreams.tv" target="_blank">redditsoccerstreams</a>. <br> This site does not host or upload any video & media files (It is for learning purpose only).<br> If you have any legal issues please contact appropriate media owners or hosters.</strong></h6>
              ${tableHtml}
            </div>
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.11.8/umd/popper.min.js" integrity="sha512-TPh2Oxlg1zp+kz3nFA0C5vVC6leG/6mm1z9+mA81MI5eaUVqasPLO8Cuk4gMF4gUfP5etR73rgU/8PNMsSesoQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.2/js/bootstrap.min.js" integrity="sha512-WW8/jxkELe2CAiE4LvQfwm1rajOS8PHasCCx+knHG0gBHt8EXxS6T6tJRTGuDQVnluuAvMxWF4j8SNFDKceLFg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
          </body>
        </html>
      `);
      } else {
        res.status(response.statusCode).send(error);
      }
    }
  );
});

// New route for API endpoint to serve data
app.get("/api/game", function (req, res) {
  request(
    "https://www.redditsoccerstreams.tv/index.php",
    function (error, response, body) {
      if (!error && response.statusCode == 200) {
        const $ = cheerio.load(body);
        const tableRows = $("tbody tr");
        const data = [];

        const generateId = (game) => {
          // Remove special characters, convert to lowercase, and replace spaces with dashes
          return game
            .replace(/[^\w\s]/gi, "")
            .toLowerCase()
            .replace(/\s+/g, "-");
        };

        tableRows.each(function () {
          const text = $(this).find("td:nth-child(1)").text();
          const date = new Date(`01/01/2000 ${text}`);
          date.setHours(date.getHours() + 3);
          const hours = date.getHours().toString().padStart(2, "0");
          const minutes = date.getMinutes().toString().padStart(2, "0");
          const formattedTime = `${hours}:${minutes}`;

          const match = $(this).find("td:nth-child(2)").text();
          const watchLink = $(this).find("td:nth-child(3) a").attr("href");

          const id = generateId(match);

          const uniqueId = uuidv4();

          data.push({
            uniqueId,
            time: formattedTime,
            game: match,
            gameImg:
              "https://res.cloudinary.com/weknow-creators/image/upload/v1705181592/reddit-soccer-streams-300x170_j7hkva.jpg",
            source: watchLink,
            gameId: id,
          });
        });

        res.json(data);
      } else {
        res.status(response.statusCode).send(error);
      }
    }
  );
});

app.listen(2000, function () {
  console.log("App listening on port 2000!");
});
